@extends('index')
@section('content')
	<div id="body">
		<h2>Instructors</h2>
		<div class="content">
			<div>
				@foreach($teachers as $teacher)
					<ul class="section">
						<li>
								<img src="{{$teacher->img}}" alt="Yoga instructor one">
								<h4>Name : {{$teacher->name}}</h4>
								<h5>Country : {{$teacher->address}}</h5>
								<h5>Phone : {{$teacher->phone}}</h5>
						</li>
					</ul>
				@endforeach
			</div>
			<img src="images/lady-in-yoga.jpg" alt="lady doing yoga" class="figure">
		</div>
		<div class="space40">&nbsp;</div>
		<div class="row">{{$teachers->render()}}</div>
	</div>
@endsection