@extends('index')
@section('content')
	<div id="body">
		<h2>Single Post</h2>
		<div class="content">
			<div class="article">
				<h3>{{$post->name}}</h3>
				<span>{{$post->time}}</span>
				<img src="/{{$post->img}}" alt="yoga lying down legs in the air">
				<p>
					{{$post->content}}
				</p>
			</div>
			<div class="sidebar">
				<h3>Recent Posts</h3>
				<ul>
					@foreach($blogs as $blog)
					<li>
						<h4><a href="/blog/{{$blog->id}}">{{$blog->name}}</a></h4>
						<span>{{$blog->time}}</span>
					</li>
					@endforeach
				</ul>
				<div class="row">{{$blogs->render()}}</div>
			</div>


		</div>
	</div>
@endsection