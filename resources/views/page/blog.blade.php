@extends('index')
@section('content')
	<div id="body">
		<h2>Blog</h2>
		<ul class="blog">
			@foreach($blogs as $blog)
			<li>
				<img src="{{$blog->img}}" alt="yoga in group">
				<h3><a href="blog/{{$blog->id}}">{{$blog->name}}</a></h3>
				<span>{{$blog->time}}</span>
				<p>
					{{$blog->descript}}
				</p>
			</li>
			@endforeach
		</ul>
		<div class="space40">&nbsp;</div>
		<div class="row">{{$blogs->render()}}</div>
	</div>
@endsection