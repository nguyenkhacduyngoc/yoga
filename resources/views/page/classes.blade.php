@extends('index')
@section('content')
	<div id="body">
		<h2>Classes</h2>
		<div class="content">
			<div>
				@foreach($classes as $class)
				<div class="section">
					<h3>{{$class->name}}</h3>
					<p>
						{{$class->descript}}
					</p>
					<span>{{$class->time}}</span>
					<span>TTHS : 10AM-11AM</span>
				</div>
				@endforeach
			</div>
			<img src="images/lady-in-yoga.jpg" alt="lady doing yoga" class="figure">
		</div>
	</div>
@endsection