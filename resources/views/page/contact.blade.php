@extends('index')
@section('content')
	<div id="body">
		<h2>Contact</h2>
		<ul>
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
		{!! Form::open(['route'=>'contact']) !!}
			{!! Form::token(); !!}
			<h3>Inquiries</h3>
			{!! Form::label('name','Name') !!}
				{!! Form::text('name',null,[
					'required',
					'id'=>'name'
				]) !!}

			{!! Form::label('email','Email') !!}
				{!! Form::email('email',null,[
					'required',
					'id'=>'email'
				]) !!}
			{!! Form::label('subject','Subject') !!}
				{!! Form::text('subject',null,[
					'required',
					'id'=>'subject'
				])!!}
			{!! Form::label('message','Message') !!}
				{!! Form::textarea('message',null,[
					'required',
					'id'=>'message',
					'cols'=>'30',
					'rows'=>'10'
				]) !!}

			{!! Form::submit('Send',[
				'id'=>'send'
			]) !!}

		{!! Form::close() !!}
	</div>
@endsection