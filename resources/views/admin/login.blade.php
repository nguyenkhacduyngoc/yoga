 <div id="body">
    <h2 id="text-login">Login</h2>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        {!! Form::open(['route'=>'login']) !!}
        {!! Form::token(); !!}

        {!! Form::label('username','User Name') !!}
        {!! Form::text('username',null,[
            'required',
            'id'=>'name'
        ]) !!}

        {!! Form::label('password','Password') !!}
        {!! Form::password('password',null,[
            'required',
            'id'=>'password'
        ]) !!}

        {!! Form::submit('Login',[
            'class'=>'btn btn-primary',
            'id'=>'btn-login'
        ]) !!}

        {!! Form::close() !!}
    </div>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '{your-app-id}',
                cookie     : true,
                xfbml      : true,
                version    : '{latest-api-version}'
            });
            FB.AppEvents.logPageView();
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    </script>
{{--AppID: 1729640023754307--}}
{{--AppSecret: 4e65dcdffc9fd15684f6f24bdabd6c3a--}}