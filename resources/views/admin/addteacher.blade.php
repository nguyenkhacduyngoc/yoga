<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Manager</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
</head>
<body>
<h2>Manage Teacher</h2>
<table border="cell" >
    <th>
    <td>id</td>
    <td>name</td>
    <td>id_class</td>
    <td>address</td>
    <td>phone</td>
    <td>img</td>
    </th>
    @foreach($teachers as $teacher)
        <tr>
            <td><button>Edit</button></td>
            <td>{{$teacher->id}}</td>
            <td>{{$teacher->name}}</td>
            <td>{{$teacher->id_class}}</td>
            <td>{{$teacher->address}}</td>
            <td>{{$teacher->phone}}</td>
            <td><img src="{{$teacher->img}}"></td>
        </tr>
    @endforeach
    </table>


    <button  class="btn-default" id="editTable" onclick="toggleAddForm()">Add Teacher</button>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
    <div  id="addForm" class="col-md-4"  style="display: none;align-content: center;margin: 10px 10px 10px 10px">
            {!! Form::open(['route'=>'add-class'
            ]) !!}
            {!! Form::token(); !!}
            <div class="form-group">
            {!! Form::label('id','ID') !!}
                {!! Form::text('id',null,[
                        'required',
                        'id'=>'id',
                        'class'=>'form-control'
                ])!!}
            </div>
            <div class="form-group">
            {!! Form::label('name','Name') !!}
                {!! Form::text('name',null,[
                        'required',
                        'id'=>'name',
                        'class'=>'form-control'
                ])!!}
            </div>
            <div class="form-group">
            {!! Form::label('id_class','ID_class') !!}
                {!! Form::text('id_class',null,[
                        'required',
                        'id'=>'id_class',
                        'class'=>'form-control'
                ])!!}
            </div>
            <div class="form-group">
            {!! Form::label('address','Address') !!}
                {!! Form::text('address',null,[
                        'required',
                        'id'=>'address',
                        'class'=>'form-control'
                ])!!}
            </div>
            <div class="form-group">
            {!! Form::label('phone','Phone') !!}
                {!! Form::text('phone',null,[
                        'required',
                        'id'=>'phone',
                        'class'=>'form-control'
                ])!!}
            </div>
            <div class="form-group">
            {!! Form::label('img','Image') !!}
                {!! Form::text('img',null,[
                        'required',
                        'id'=>'img',
                        'class'=>'form-control'
                ])!!}
            </div>
            {!! Form::submit('Add',[
				'id'=>'add'
			]) !!}
            </div>
            {!! Form::close() !!}
    <script !src="">
        function toggleAddForm() {
            var addForm =document.getElementById('addForm');
            addForm.style.display = (addForm.style.display == "div") ?"none" :"div";
        }
    </script>
</body>
</html>

