<?php

use Illuminate\Database\Seeder;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(classSeeder::class);
//        $this->call(teacherSeeder::class);
//        $this->call(adminSeeder::class);
//        $this->call(blogSeeder::class);
        $this->call(commentSeeder::class);
    }
}

class classSeeder extends Seeder{
    public function run (){
        DB::table('classes')->insert([
            ['id'=>1,'name'=>'Beginners','id_teacher'=>1,'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent in neque condimentum, dignissim libero sit amet, blandit felis.'],
            ['id'=>2,'name'=>'Intermediate','id_teacher'=>2,'description'=>'Cras dignissim est sed lorem suscipit, ut ultrices dolor tempus. Cras venenatis gravida scelerisque. Aenean sit amet massa dui'],
            ['id'=>3,'name'=>'Advanced','id_teacher'=>3,'description'=>'Quisque non quam lorem. Nulla eu placerat leo. Suspendisse eros risus, viverra sed fermentum vitae, gravida ac tellus.']
        ]);
    }
}

class teacherSeeder extends Seeder{
    public function run (){
        DB::table('teachers')->insert([
            ['id'=>1,'name'=>'Belle Humpfree','id_class'=>1,'address'=>'USA','phone'=>'123456789','img'=>'images/instructor1.jpg'],
            ['id'=>2,'name'=>'Jacob Collern','id_class'=>2,'address'=>'USA','phone'=>'123456789','img'=>'images/instructor2.jpg'],
            ['id'=>3,'name'=>'Edward Blance','id_class'=>2,'address'=>'Holland','phone'=>'123456789','img'=>'images/instructor3.jpg'],
            ['id'=>4,'name'=>'Carrie Waldorf','id_class'=>3,'address'=>'France','phone'=>'123456789','img'=>'images/instructor4.jpg']
        ]);
    }
}

class blogSeeder extends Seeder{
    public function run (){
        DB::table('blogs')->insert([
            ['id'=>1,'name'=>'Summer Yoga Classes','content'=>'Aliquam elementum varius adipiscing. Phasellus vulputate arcu eu scelerisque egestas. Duis lacinia, tellus sed congue porta, tortor felis ornare mi, in eleifend dolor odio ac lacus. Curabitur id nisi ornare, viverra lorem sed, viverra ligula. Donec eu nibh tempus, semper velit sit amet, blandit odio. Phasellus id condimentum sapien, sit amet venenatis sem.
Nam luctus nunc ut orci posuere ultricies. Donec id ante nec nunc laoreet vestibulum. Nulla in tortor non risus pharetra suscipit. Suspendisse nec diam gravida, scelerisque magna eget, dignissim enim. Sed eget diam facilisis justo elementum aliquet quis ut felis. Vestibulum sus cipit nibh consectetur massa volutpat, vel euismod lectus blandit.','img'=>'images/group-yoga.jpg','description'=>'Duis ultrices tortor non felis convallis bibendum. Maecenas diam velit, sollicitudin at imperdiet ac, consectetur non nibh. Etiam eget dapibus nulla. Nulla placerat mauris ut elit placerat luctus. Aliquam porttitor leo non nisl scelerisque sollicitudin.'],
            ['id'=>2,'name'=>'Inner peace','content'=>'Aliquam elementum varius adipiscing. Phasellus vulputate arcu eu scelerisque egestas. Duis lacinia, tellus sed congue porta, tortor felis ornare mi, in eleifend dolor odio ac lacus. Curabitur id nisi ornare, viverra lorem sed, viverra ligula. Donec eu nibh tempus, semper velit sit amet, blandit odio. Phasellus id condimentum sapien, sit amet venenatis sem.
Nam luctus nunc ut orci posuere ultricies. Donec id ante nec nunc laoreet vestibulum. Nulla in tortor non risus pharetra suscipit. Suspendisse nec diam gravida, scelerisque magna eget, dignissim enim. Sed eget diam facilisis justo elementum aliquet quis ut felis. Vestibulum sus cipit nibh consectetur massa volutpat, vel euismod lectus blandit.','img'=>'images/yoga-concentrating.jpg','description'=>'Duis ultrices tortor non felis convallis bibendum. Maecenas diam velit, sollicitudin at imperdiet ac, consectetur non nibh. Etiam eget dapibus nulla. Nulla placerat mauris ut elit placerat luctus. Aliquam porttitor leo non nisl scelerisque sollicitudin.'],
            ['id'=>3,'name'=>'A Strong and Flexible Body','content'=>'Aliquam elementum varius adipiscing. Phasellus vulputate arcu eu scelerisque egestas. Duis lacinia, tellus sed congue porta, tortor felis ornare mi, in eleifend dolor odio ac lacus. Curabitur id nisi ornare, viverra lorem sed, viverra ligula. Donec eu nibh tempus, semper velit sit amet, blandit odio. Phasellus id condimentum sapien, sit amet venenatis sem.
Nam luctus nunc ut orci posuere ultricies. Donec id ante nec nunc laoreet vestibulum. Nulla in tortor non risus pharetra suscipit. Suspendisse nec diam gravida, scelerisque magna eget, dignissim enim. Sed eget diam facilisis justo elementum aliquet quis ut felis. Vestibulum sus cipit nibh consectetur massa volutpat, vel euismod lectus blandit.','img'=>'images/lying-yoga.jpg','description'=>'Duis ultrices tortor non felis convallis bibendum. Maecenas diam velit, sollicitudin at imperdiet ac, consectetur non nibh. Etiam eget dapibus nulla. Nulla placerat mauris ut elit placerat luctus. Aliquam porttitor leo non nisl scelerisque sollicitudin.']
        ]);
    }
}

class adminSeeder extends Seeder
{
    public function run()
    {
        DB::table('admins')->insert([
            ['id' => 1, 'username' => 'duyngoc', 'password' => '123456'],
            ['id' => 2, 'username' => 'ngoc', 'password' => '12345'],
            ['id' => 3, 'username' => 'duyngoc1', 'password' => '1234567']
        ]);
    }
}
class commentSeeder extends Seeder
{
    public function run (){
        DB::table('comments')->insert([
            ['id'=>1 , 'id_post'=>1,'content'=>'Good post','id_user'=>'duyngoc',],
        ]);
    }
}