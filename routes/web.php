<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//PAGE----------------------------------------

Route::get('/',[
    'uses'=>'PageController@getHomePage'
]);

Route::get('homepage',[
	'as'=>'homepage',
	'uses'=>'PageController@getHomePage'
]);

Route::get('about',[
	'as'=>'about',
	'uses'=>'PageController@getAbout'
]);

Route::get('blog',[
	'as'=>'blog',
	'uses'=>'PageController@getBlog'
]);

Route::get('classes',[
	'as'=>'classes',
	'uses'=>'PageController@getClasses'
]);

Route::get('contact',[
	'as'=>'contact',
	'uses'=>'PageController@getContact'
]);

Route::post('contact',[
    'as'=>'contact-response',
    'before' => 'csrf',
    'uses'=>'PageController@sendResponse'
]);

Route::get('instructors',[
	'as'=>'instructors',
	'uses'=>'PageController@getInstructors'
]);

Route::get('blog/{id}',[
	'as'=>'singlepost',
	'uses'=>'PageController@getSinglePost'
]);

//ADMIN--------------------------------------------------------------
Route::get('adminhome',[
    'as'=>'adminhome',
    'uses'=>'AdminController@getHome'
])->middleware('checkauthen');
Route::get('login',[
    'as'=>'login',
    'uses'=>'AdminController@getLogin'
]);
Route::post('login',[
    'as'=>'login-submit',
    'before' => 'csrf',
    'uses'=>'AdminController@submitLogin'
]);
Route::get('logout',[
    'as'=>'logout',
    'uses'=>'AdminController@getLogout'
]);
Route::get('add-class',[
    'as'=>'add-class',
    'uses'=>'AdminController@getAddClass'
])->middleware('checkauthen');;
Route::get('add-teacher',[
    'as'=>'add-teacher',
    'uses'=>'AdminController@getAddTeacher'
])->middleware('checkauthen');;
Route::post('add-teacher',[
    'as'=>'add-teacher-submit',
    'uses'=>'AdminController@addTeacher'
])->middleware('checkauthen');;;
Route::get('post-blog',[
    'as'=>'post-blog',
    'uses'=>'AdminController@getPostBlog'
])->middleware('checkauthen');;
Route::get('reply-email',[
    'as'=>'reply-email',
    'uses'=>'AdminController@getReplyEmail'
])->middleware('checkauthen');;
Route::get('/home',
    'HomeController@index')
    ->name('home')
    ->middleware('checkauthen');;
