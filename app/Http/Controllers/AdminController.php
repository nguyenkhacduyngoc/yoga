<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Teachers;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function getHome(){
        $admins=  Admin::all();
        return view('admin.adminhome',compact('admins'));
    }
    public function getLogin(){
        session_start();
        if(isset($_SESSION['username'])&&isset($_SESSION['password'])){
            return redirect('adminhome');
        }
        return view('admin.login');
    }

    public function getLogout(){
        session_start();
        unset($_SESSION['username']);
        unset($_SESSION['password']);
        return redirect('./');
    }
    public function submitLogin(Request $req){
        $admins = Admin::all();
        session_start();
        foreach ($admins as $admin){
            if(($req->username == $admin->username) && ($req->password==$admin->password)){
                $_SESSION['username'] = $req->username;
                $_SESSION['password'] = $req->password;
                return redirect('adminhome');
            }
        }
        return view('admin.login');



    }
    public function getAddClass(){
        return view('admin.addclass');
    }
    public function getAddTeacher(){
        $teachers =Teachers::all();

        return view('admin.addteacher',compact('teachers'));
    }
    public function addTeacher(){

    }
    public function editTeacher(){

    }
    public function getPostBlog(){
        return view('admin.postblog');
    }
    public function getReplyEmail(){
        return view('admin.replyemail');
    }
}
