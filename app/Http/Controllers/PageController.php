<?php

namespace App\Http\Controllers;
use App\Classes;
use App\Response;
use App\Teachers;
use App\Blogs;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function getHomePage(){
    	return view('page.homepage');
    }

    public function getAbout(){
    	return view('page.about');
    }

    public function getBlog(){
        $blogs= Blogs::paginate(3);

    	return view('page.blog',compact('blogs'));
    }

    public function getClasses(){
        $classes = Classes::all();
    	return view('page.classes',compact('classes'));
    }

    public function getContact(){
    	return view('page.contact');
    }
    public function sendResponse(Request $request){
        $response = new Response();
        $response->name = $request->name;
        $response->email =$request->email;
        $response->subject =$request->subject;
        $response->mess =$request->message;
        $response->save();
        return redirect()->back();
    }
    public function getInstructors(){
        $teachers = Teachers::paginate(3);
    	return view('page.instructors',compact('teachers'));
    }

    public function getSinglePost($id){
        $post = Blogs::find($id);
        $blogs = Blogs::paginate(2);
    	return view('page.singlepost',compact('post','blogs'));
    }
}
