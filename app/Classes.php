<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    protected $table= 'classes';

    public function teachers(){
    	return $this->hasMany('App\Teachers','id_teacher','id');
    }
}
