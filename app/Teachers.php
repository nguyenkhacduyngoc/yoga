<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teachers extends Model
{
    protected $table= 'teachers';

    public function classes(){
    	return $this->belongsTo('App/Classes','id_classes','id');
    }
}
